---
title: "Airbnb_Shiny"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(dplyr)
library(readr)
library(ggmap)
library(ggplot2)
library(VIM)
library(shiny)
```


```{r,include=FALSE}
more_viewed <- read.csv("https://www.dropbox.com/s/c42msksls4mmeis/more_viewed.csv?dl=1")
```



```{r,include=FALSE}
model_data <- more_viewed %>% select(neighbourhood_cleansed,property_type,room_type,accommodates,bathrooms,bedrooms,price,cancellation_policy)

sample_data <- sample(2,nrow(model_data),replace=TRUE,prob=c(0.7,0.3))
train_data <- model_data[sample_data==1,] #Train


proptertylist <- train_data$property_type
policylist <- train_data$cancellation_policy

test_data <- model_data[sample_data==2,] #Test
test_data <- test_data %>% filter(property_type %in% proptertylist)
test_data <- test_data %>% filter(cancellation_policy %in% policylist)

mod1 <- lm(data = train_data, price ~.,na.action=na.exclude) 

```




```{r,include=FALSE}
ui <- fluidPage(
  
  # Sidebar layout with a input and output definitions 
  sidebarLayout(
    
    # Inputs
    sidebarPanel(
      
      # Select variable for y-axis
      selectInput(inputId = "y",
                   label = "View:",
                   choices = c("price","review_scores_rating"),
                   selected = "price"),
      # Select variable for x-axis
      
     selectInput(inputId = "x",
                   label = "Group by:",
                   choices = c("neighbourhood_cleansed","property_type"),
                   selected = "neighbourhood_cleansed"),

     
     selectInput(inputId = "z", 
                  label = "Color by:",
                  choices = c("room_type","bathrooms","bedrooms"), 
                  selected = "room_type"),
     
           
      selectInput(inputId = "neighbourhood_cleansed", 
                  label = "User-Neighbourhood:",
                  choices = levels(train_data$neighbourhood_cleansed),
                  selected = "Hollywood"),
      
     selectInput(inputId = "property_type", 
                  label = "User-Property Type:",
                  choices = levels(train_data$property_type), 
                  selected = "Apartment"),
     
     selectInput(inputId = "room_type", 
                  label = "User-Room Type:",
                  choices = levels(train_data$room_type), 
                  selected = "Private room"),
     
     selectInput(inputId = "accommodates", 
                  label = "User-Accommodates:",
                  choices = c(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16), 
                  selected = 1),
     
     
     selectInput(inputId = "bathrooms", 
                  label = "User-Bathrooms:",
                  choices = c(0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,7), 
                  selected = 1),
     
     selectInput(inputId = "bedrooms", 
                  label = "User-Bedrooms:",
                  choices = c(0,1,2,3,4,5,6,7,8),
                 selected = 1),
     
     selectInput(inputId = "cancellation_policy", 
                  label = "User-Cancellation policy:",
                  choices = levels(train_data$cancellation_policy), 
                  selected = "flexible")
     ),     
    
    # Outputs
    mainPanel(
      plotOutput(outputId = "histplot"),
      
      h4("Suggested Price:"),
      verbatimTextOutput(outputId = "value_recommendation")

    )
  )
)

```


```{r,include=FALSE}
server <- function(input, output) {

  # Create scatterplot object the plotOutput function is expecting
  output$histplot <- renderPlot({
    ggplot(data = more_viewed, aes_string(x = input$x, y = input$y, color = input$z)) +
      geom_point() +
       theme(axis.text.x = element_text( vjust = 0.5,   hjust = 0.5, angle = 90))
  })
 
  
user_defined <- reactive({
      data.frame(
        neighbourhood_cleansed = req(input$neighbourhood_cleansed),
        property_type = input$property_type,
        room_type = input$room_type,
        accommodates = as.integer(input$accommodates),
        bathrooms = as.integer(input$bathrooms),
        bedrooms = as.integer(input$bedrooms),
       cancellation_policy= input$cancellation_policy)
  })

    output$value_recommendation <-  renderPrint(predict(mod1,user_defined())) 
  
}
```


```{r}

shinyApp(ui = ui, server = server)

```
